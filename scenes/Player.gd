extends KinematicBody2D

# How fast the player will move (pixels/sec).
export (int) var max_speed = 400
export (float) var start_acceleration = 0.065
export (float) var stop_friction = 0.01
export (int) var touch_distance_limit = 20
# Size of the game window.
var screen_size
var speed = 0

# The click/touch position
var touch_point = Vector2()
var velocity = Vector2()
var released = true

# Signals
signal hit


func _ready():
  # hide()
  screen_size = get_viewport_rect().size
  $Burst.hide()

func _process(delta):
  if Input.is_action_pressed('click'):
    released = false
    touch_point = get_global_mouse_position()
    if speed == 0:
      speed = 1

    speed += (speed * start_acceleration)

    if speed >= max_speed:
      speed = max_speed

    $Burst.show()
    $Burst.play('burst')
    velocity = (touch_point - position).normalized() * speed

  if released and speed > 0:
    speed -= (speed * stop_friction)

    if speed <= 0:
      speed = 0

    $Burst.play('cooldown')
    velocity = velocity.normalized() * speed


func _input(event):
  if event.is_action_released('click'):
    released = true

func _physics_process(delta):
  if (touch_point - position).length() > touch_distance_limit:
    rotation = velocity.angle()
    velocity = move_and_slide(velocity)


func _on_Player_body_entered(body):
  hide()
  emit_signal('hit')
  $CollisionShape2D.set_deferred('disabled', true)


func start(pos):
  position = pos
  show()
  $CollisionShape2D.disabled = false


func _on_Burst_animation_finished():
  if $Burst.animation == 'cooldown':
    $Burst.hide()
