extends Node

export (PackedScene) var Astroid1
export (PackedScene) var Astroid2
export (PackedScene) var Astroid3
export (PackedScene) var Astroid4
export (int) var astroid_min_speed = 100
export (int) var astroid_max_speed = 400

var astroids = []

var score

func _ready():
  # Sense are loaded now
  astroids = [Astroid1, Astroid2, Astroid3, Astroid4]
  new_game()

func new_game():
    score = 0
    #$Music.play()
    # $HUD.update_score(score)
    # $HUD.show_message("Get Ready")
    $Player.start($StartPosition.position)
    $StartTimer.start()
    $AstroidTimer.start()


func _on_AstroidTimer_timeout():
  # Choose a random location on Path2D.
  $ScreenEadge/Follow.set_offset(randi())
  # Create an astroid instance and add it to the scene.
  var astroid = astroids[randi() % 4].instance()
  add_child(astroid)

  #$HUD.connect("start_game", astroid, "_on_start_game")

  # Set the astroid's direction perpendicular to the path direction.
  var direction = $ScreenEadge/Follow.rotation + PI / 2
  # Set the astroid's position to a random location.
  astroid.position = $ScreenEadge/Follow.position

  # Add some randomness to the direction.
  direction += rand_range(-PI / 4, PI / 4)
  astroid.rotation = direction

  # Set the velocity (speed & direction).
  astroid.linear_velocity = Vector2(rand_range(astroid_min_speed, astroid_max_speed), 0)
  astroid.linear_velocity = astroid.linear_velocity.rotated(direction)
